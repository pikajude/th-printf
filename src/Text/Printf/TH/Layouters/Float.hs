module Text.Printf.TH.Layouters.Float where

import           Data.Maybe
import           Data.Char                      ( intToDigit )
import           Numeric                        ( floatToDigits )
import           GHC.Float                      ( roundTo )
import           Prelude
import           Data.Bits
import           Data.Ix                        ( inRange )

import           Numeric.Extra
import qualified Text.Printf.TH.Parse.Flags    as F
import           Text.Printf.TH.Builder
import           Text.Printf.TH.Layout

type Layouter a buf = Maybe Int -> Maybe Int -> F.Flags -> a -> buf

fixed width prec_ flags f' =
  layout width (signedPrefix flags f') (F.justify flags)
    $ case splitAt exp1 digs1 of
        ([], []) -> str "0." <> times prec '0'
        (xs, []) -> build intToDigit xs
        ([], xs) -> str "0." <> times (-exp1) '0' <> build intToDigit xs
        (xs, ys) -> build intToDigit xs <> char '.' <> build intToDigit ys
 where
  f             = abs f'
  (digs1, exp1) = floatToRounded 10 roundSize f
  prec          = fromMaybe 6 prec_
  (_, exp0)     = floatToDigits 10 f
  roundSize     = exp0 + prec

sciU = sci True
sciL = sci False

sci upper width prec flags f' =
  layout width (signedPrefix flags f') (F.justify flags) $ mconcat
    [ char (intToDigit whole)
    , if not (null part) || useAlt then char '.' else mempty
    , build intToDigit part
    , eChar
    , char (if exp1 >= 0 then '+' else '-')
    , justifyRight (Just 2) '0' (showIntAtBase 10 intToDigit (abs exp1))
    ]
 where
  useAlt         = F.prefix flags
  eChar          = if upper then char 'E' else char 'e'
  f              = abs f'
  (digs0, exp0)  = floatToRounded 10 realPrec f
  (whole : part) = digs0
  exp1           = exp0 - 1
  realPrec       = if prec == Just 0 then 0 else 1 + fromMaybe 6 prec

genericU = generic True
genericL = generic False

-- fuck generic
-- between 0.000001 and 999999.0 use fixed, otherwise scientific
-- unless the specified precision is lower than the exponent of the float
-- in which case force using scientific
-- if there are fewer than 6 significant digits and # is specified,
-- pad out the sig digits with zeroes until it reaches 6
generic upper width prec flags f =
  let inner | useSci    = showSci
            | otherwise = showFixed
  in  layout width (signedPrefix flags f) (F.justify flags) inner
 where
  useAlt     = F.prefix flags
  (_, e0)    = floatToDigits 10 f
  useSci     = not (inRange (1 - realPrec, realPrec) e0)
  realPrec   = maybe 6 (max 1) prec
  eChar      = if upper then char 'E' else char 'e'
  dropZeroes = reverse . dropWhile (== 0) . reverse

  showFixed  = mconcat
    [ if null whole then char '0' else build intToDigit whole
    , if null part1 && not useAlt then mempty else char '.'
    , times (-e0) '0'
    , build intToDigit part1
    ]
   where
    (whole, part0) = uncurry (flip splitAt) (floatToRounded 10 realPrec f)
    part1          = if useAlt then part0 else dropZeroes part0

  showSci = mconcat
    [ char (intToDigit s1)
    , if null s0 && not useAlt then mempty else char '.'
    , build intToDigit s0
    , times padLength '0'
    , eChar
    , char (if e1 >= 0 then '+' else '-')
    , justifyRight (Just 2) '0' (showIntAtBase 10 intToDigit (abs e2))
    ]
   where
    (s1 : s2, e1) = floatToRounded 10 realPrec f
    s0            = dropZeroes s2
    padLength     = realPrec - length s0 - 1
    e2            = e1 - 1

hexUpper = hex True
hexLower = hex False

hex upper width prec flags f =
  layout width (Just $ str (if upper then "0X" else "0x")) (F.justify flags)
    $  char (intToDigit d1)
    <> (if not (null rounded) || F.prefix flags then char '.' else mempty)
    <> build (if upper then intToDigitUpper else intToDigit) rounded
    <> char (if upper then 'P' else 'p')
    <> char (if exp1 >= 0 then '+' else '-')
    <> showIntAtBase 10 intToDigit (abs exp1)
 where
  (d0 : digs0, exp0)   = floatToDigits 2 f
  d1                   = d0 + overflow
  rounded              = drop overflow rounded'
  (overflow, rounded') = case prec of
    Just n  -> roundTo 16 n hdigs0
    Nothing -> (0, hdigs0)
  exp1   = exp0 - 1
  hdigs0 = asHex digs0
  asHex (a : b : c : d : xs) =
    (a <<< 3 .|. b <<< 2 .|. c <<< 1 .|. d) : asHex xs
  asHex [a, b, c] = [a <<< 3 .|. b <<< 2 .|. c <<< 1]
  asHex [a, b]    = [a <<< 3 .|. b <<< 2]
  asHex [a]       = [a <<< 3]
  asHex []        = []

floatToRounded base prec f = (digs1, e0 + overflow0)
 where
  (digs0    , e0   ) = floatToDigits (fromIntegral base) f
  (overflow0, digs1) = case roundTo base prec digs0 of
    -- roundTo x 0 digs can produce an empty list of digits if digs doesn't round up
    -- e.g. roundTo 10 0 [6] is (1, [1]) but roundTo 10 0 [4] is (0, [])
    -- when in reality it should be (0, [4])
    (_, []) -> roundTo base 1 digs0
    xs      -> xs

(<<<) = shiftL
infixl 8 <<<
